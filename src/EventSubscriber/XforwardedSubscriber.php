<?php

namespace Drupal\x_forwarded_for\EventSubscriber;

use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\HttpKernel\Event\FilterResponseEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation;
use Drupal\Component\Utility\Html;
use Drupal\Component\Utility\UrlHelper;
use Drupal\Core\Config\ConfigFactoryInterface;

/**
 * Subscribing an event.
 */
class XforwardedSubscriber implements EventSubscriberInterface {

  /**
   * Drupal's settings manager.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $config;

  /**
   * XframeSubscriber constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The factory for configuration objects.
   */
  public function __construct(ConfigFactoryInterface $config_factory) {

  }

  /**
   * Executes actions on the respose event.
   *
   * @param \Symfony\Component\HttpKernel\Event\FilterResponseEvent $event
   *   Filter Response Event object.
   */
  public function onKernelResponse(FilterResponseEvent $event) {
    // Add the x-forwarded-for response header with the configured directive.
    $response = $event->getResponse();
    $response->headers->set('X-Forwarded-For', \Drupal::request()->getClientIp());
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    // Adds the event in the list of KernelEvents::RESPONSE with priority -10.
    $events[KernelEvents::RESPONSE][] = ['onKernelResponse', -10];
    return $events;
  }

}
